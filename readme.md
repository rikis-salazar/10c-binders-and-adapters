# Using `C++11`'s `std::bind`

This is a brief handout that explains some of the ways in which C++11's `bind`
function replaces some of the _older-and-more-limited_ adapters and binders
available in the `C++98` Standard Library, namely `ptr_fun`, `mem_fun`,
`mem_fun_ref`, `bind1st`, and `bind2nd`.

Although `bind` is certainly a _better-and-more-complex_ function than, say
`ptr_fun` for instance, the ideas behind both implementations are quite similar,
namely they both create a function object that contains a pointer to a function,
as well as member variables that store values for some of the parameters. 

As we've seen in class, a common use of _binders_ (e.g., `bind1st`, `bind2nd`,
etc.) is to create function objects that can be used with _generic algorithms_
like `for_each` to apply a function to elements in a container.


## Libraries and namespaces needed to access `bind`

Assuming you have a `C++11` compiler, the following `#include` and `using`
directives allow you to access `bind` as well as a special set of
_placeholders_.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <functional>   // <-- Required

// Optional, but they might make your life easier
using namespace std;    // bind, ref vs std::bind, std::ref 
using namespace std::placeholders;  // Needed for _1, _2, etc.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## Creating a function object with `bind`

As mentioned above, `bind` is a complex template function that creates and
returns a function object that _wraps_ a supplied function in the form of a
function pointer. Its basic form is: `bind( function pointer, bound arguments )`

For example, if we create a function that adds three integer values and returns
the result of the addition

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
int add3ints( int a, int b, int c ){
    int result = a + b + c;
    cout << a << '+' << b << '+' << c << '=' << result << "\n"; 
    return result;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

and if `n1`, `n2`, and `n3` are three integers, the statement

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
auto f =  bind(add3ints,n1,n2,n3); 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**binds** the values of `n1`, `n2`, and `n3` to the newly created function
object `f`. Since, all of the function parameters are now _bound_ or _attached_
to `f`, the statement

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
int sum = f();
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

results in a call to the member function `operator()`. Moreover, the `bind`
statement above stored the contents of `n1`, `n2`, and `n3`, as well as the
functionality of `add3ints` inside the function object `f`. The overall result
is the same as a direct call to `add3ints` as in the statement

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
int sum = add3ints(n1,n2,n3);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Granted, this is a very silly [rather stupid] example. But it illustrates the
general mechanism that is at play when using `bind`.

> Note(s):   
>
> * There are two set of arguments here. The ones required by `add3ints` as well
>   as the ones required by `bind`. We will use the term **function arguments**
>   for the former set, and **bound arguments** for the latter set.
>
> * The number of _bound arguments_ must match the number of _function
>   arguments_ and their order must correspond as well. The first bound argument
>   corresponds to the first function argument, the second to the second, and so
>   on.
>
> * The _bound arguments_ can also be literal values as in the statement
>   `bind(add3ints,1,10,100)()`, which creates a temporary function object and
>   calls the member `operator()`. 


### Subtle point: _pass by reference_ arguments

When `bind` creates a function object, the _bound parameters_ are passed by
value [i.e., copied]. This is because the mechanism that is used "behind the
scenes", is the same one that lambda functions use, namely, the creation of a
functor whose `operator()` is a `const` member. So, if the function that is
being _wrapped_ takes reference parameters, the modifications will be applied
to the copies stored in the functions object, and not to the original values.
This clearly poses a problem if the intention is to modify the bound parameters.
Fortunately, there is a way to solve this problem: the use of the
_reference wrapper class_ `std::ref`.

For example, consider the function

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void modify2ndAnd3rd(int a, int& b, int& c){
    b *= 2;
    c *= 3;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The statement `bind( modify2ndAnd3rd, n3, n2, n1 )()` has no effect on the
_bound parameters_, whereas the statement

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
bind( modify2ndAnd3rd, n3, n2, std::ref(n1) )()
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

triples the original value of `n1` and leaves `n2` in its original
state.

> Can you think of an everyday object that would benefit from `std::ref`?
> _Hint:_ stream objects cannot be passed by value to functions.


## Placeholders

Unlike older binders (i.e., `bind1st` and `bind2nd`), `bind` provides more
flexibility by

* allowing the user to bind one or more parameters, and
* allowing the user to select the ordering of the parameters [assuming 
  compatible types].

For example, consider the equivalent statements

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
bind(add3ints, _2, n2, _1)(n3,n1);
bind(add3ints, _1, n2, _2)(n1,n3);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

In both cases, the underlying function call is the same, namely
`add3ints(n1,n2,n3)`. The placeholders `_1`, `_2` in the bound argument list are
special symbols defined in the namespace `std::placeholders`. They determine the
order in which the parameters `n1`, and `n2` will be sent to `add3ints`. In the
first statement above, the first bound parameter will be replaced by `n1` which
appears second in the list of _call parameters_; whereas in the second
statement, the first bound parameter will also be replaced by `n1` because it
now appears first in the list of _call parameters_.  Any mixture of placeholders
and ordinary arguments can appear in a `bind` statement, as long as the number
of items in the bound argument list equals the number of function arguments. 

> What do the following _extreme_ examples do?
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
> bind(add3ints, n1, _5, _2)(1, n3, 10, 100, n2);
> bind(add3ints, _1, _1 _1)(n2); 
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### Subtle point: _pass by reference_ arguments and placeholders

The obvious question here is: Can we use placeholders when the function used
inside the bind statement expects reference parameters? The answer is: _Yes!
But..._ placeholders implicitly use `std::ref`. One has to be careful here, if
the argument sent instead of the placeholder is modifiable (_lvalue_), then the
value will change. On the other hand, if it is a constant or a function call
(_rvalue_), it will result in a compile error.

For example, the call

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
bind(modify2ndAnd3rd, _3, _2, _1)(10, n3, n1); // Oops!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

fails because `_1` sends the constant `10` to a function that is set to change
this value. On a similar note, the call

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
bind(modify2ndAnd3rd, _3, _2, _1)(n2, n1 + n2, 1); // Oops again!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

fails because `_2` sends the result of `n1 + n2` to a function that is set to
change this value. However, the call 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
bind(modify2ndAnd3rd, _1, _2, _3)(1 + n1, n2, n3);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

is OK because `1 + n1` is sent [to `modify2ndAnd3rd`] _by value_ via `_1`.


## `bind` and generic algorithms

As it was the case with our example using `ptr_fun` and `bind2nd`, generic
algorithms and `bind` can be combined together. When an algorithm like
`for_each` is executed, the dereferenced iterator will be passed to the
function object returned by `bind`. In this case one needs to make sure that
said function object leaves the correct number of parameters unbound: one
unbound parameter for the case of a predicate; two unbound parameters for the
case of a comparator or a binary function; etc.

For a more specific example, assuming `int_vec` is a `std::vector<int>` object
that has been already populated, the statement

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
for_each( int_vec.begin(), int_vec.end(), bind( add3ints, -10, -5, _1 ) );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

will subtract `15` from every element stored in `int_vec`. Notice that as one
would expect, there is no trailing set of parenthesis after the call to `bind`,
this is because `for_each` applies `operator()` inside its body.

> Question: What does the following snippet do?
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
> for_each( int_vec.begin(), int_vec.end(), 
>           bind( modify2ndAnd3rd, _1, _1, _1 ) );
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
>
> What about this one?
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
> vector<int> v = {1, 2, 3, 4, 5}; 
>
> int mystery = 
>     accumulate( v.begin(), v.end(), 0,
>         bind( []( int c, int b, int a ){ return a + b*b + c*c*c; },
>             0, _2, _1
>         )   
>     );
>
> cout << "Mystery value: " << mystery << ".\n";
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

--- 

Included files:

|**File**|**Description**|**URL**|
|:------------:|:------------------------------------------------|:------------|
| [`readme.md`][read-me] | _readme_ file (`MarkDown` format).| `N/A` |
| [`readme.md.pdf`][read-me-pdf] | _readme_ file (`pdf` format). | `N/A` |
| [`00-bind.cpp`][00-example] | `std::bind` examples (mostly from the _readme_ file).| [`cpp.sh/4avds`][00-url] |
| [`01-squ....cpp`][01-example] | Mysterious use of `bind` + a lambda function.| [`cpp.sh/5hvlo`][01-url] |
| [`Ad.../r...md`][bind-adapt-md] | How adapters and binders work (`MarkDown`). | `N/A` |
| [`Ad.../r...pdf`][bind-adapt-pdf] | How adapters and binders work (`pdf`). | `N/A` |


[read-me]: readme.md
[read-me-pdf]: readme.md.pdf
[00-example]: 00-bind.cpp
[01-example]: 01-squares.cpp
[bind-adapt-md]: AdvancedExamples/ref-bind2nd.md
[bind-adapt-pdf]: AdvancedExamples/ref-bind2nd.md.pdf

[00-url]: http://cpp.sh/4avds
[01-url]: http://cpp.sh/5hvlo

