% How binders and adapters work
% Elmer Homero (rikis-salazar)
% December 8, 2017

# The goal statement

## What we see

 *  Assume `custom_fun` is a user-defined (_i.e.,_ non-standard) function.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <list>         // any container will do
#include <algorithm>    // std::for_each
#include <functional>   // std::ptr_fun, std::bind2nd
using namespace std;    // for convenience

int custom_fun( double d, bool b ){ ... }
list<int> the_list;
...

for_each( the_list.begin(), the_list.end(),
	  bind2nd( ptr_fun( custom_fun ), false )
	);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Basic remarks

 -  For the inner `bind2nd`:

    * `bind2nd` knows how to deal with standard binary functions (_e.g.,_
      `std::multiplies<T>`), as well as unary functions.
    *  Custom made functions need to be adapted (wrapped up), so that
       `bind2nd` understands them.

 -  For the outer `for_each`:

    * `for_each` requires a _unary function_, as well as a _range_. 
    * `bind2nd` _fixes_ the second parameter of `custom_fun`, effectively
      converting it into a function that `for_each` accepts.


# The ideas

## For the adapter

 -  Under the hood, `ptr_fun`

    * Uses a [dumb] template functor class to _store_ the function pointer
      associated to the function that is to be wrapped.
    * Overloads the functor's `operator()` function so that it "acts like"
      the function that is being wrapped.
    * Uses a [smart] template function

        i.  to extract information about the types being passed as
            parameters, and
        i.  to create an instance of the [dumb] functor class with the
            info extracted via the function parameters.


## For the binder

 -  Under the hood, `bind2nd`

    * Uses a [dumb] template functor class to _store_ 

        i. the binary function, and
        i. the value of the fixed parameter.

    * Overloads the functor's `operator()` function so that it "acts like"
      a unary function; in reality there is still a call to the binary
      function "stored" in the class.

    * Uses a [smart] template function

        i.  to extract information about the types being passed as
            parameters, and
        i.  to create an instance of the [dumb] functor class with the
            info extracted via the function parameters.

# The [technical] details

## Note:

> You can safely skip this section when studying for the final. However, you
> should at some point (after the exam for instance) go through the code
> presented here. I am positive you can benefit a lot if you are able to
> understand some of the fine details required to make these types of
> classes work together.


## For the adapter

 -  We do not have to start from scratch. There is a class (struct) that
    holds typedefs for binary functions:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template<typenane ARG1, typename ARG2, typename RESULT>
struct binary_function{
    // Just a set of standard names that everyone can use. 
    typedef ARG1    first_argument_type;
    typedef ARG2    second_argument_type;
    typedef RESULT  result_type;
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## ... adapter (cont)

 -  Next, we need the [dumb] functor class...

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template<typenane ARG1, typename ARG2, typename RESULT>
class ptr_to_bin_fun : public binary_function<ARG1,ARG2,RESULT>{
  private:
    RESULT (*f_ptr_member_field)(ARG1, ARG2);
  public:
    explicit
    ptr_to_bin_fun( RESULT (*f_ptr_param)(ARG1, ARG2) )
      : f_ptr_member_field( f_ptr_param ) { }

    RESULT operator()( ARG1 a, ARG2 b ) const {
        return f_ptr_member_field(a,b);
    }
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## ... adapter (cont)

 -  ... and the [smart] function template `ptr_fun`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template<typename ARG1, typename ARG2, typename RESULT>
// Write code in place
inline
// Return type and function name
ptr_to_bin_fun<ARG1, ARG2, RESULT> ptr_fun(
    // Function parameter
    RESULT (*fun)(ARG1, ARG2) ) {

    // Return instance of the template functor
    return ptr_to_bin_fun<ARG1, ARG2, RESULT>(fun);
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## For the binder

 -  We need the [dumb] functor class...

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template<typename OPERATION> class binder2nd {
  private:
    OPERATION oper;
    typename OPERATION::second_argument_type fixed_val;
  public:
    binder2nd( const OPERATION& op,
        const typename OPERATION::second_argument_type& val ) 
          : oper(op), fixed_val(val) { }
    typename OPERATION::result_type operator()( const typename
        OPERATION::first_argument_type& first ) const {
            return oper(first,fixed_val);
    }
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## ... binder (cont)

 -  ... and the [smart] function template `bind2nd`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template<typename OPERATION, typename T>
inline
binder2nd<OPERATION> bind2nd( const OPERATION& op,
                              const T& a ) {
    return binder2nd<OPERATION>( 
               op, 
               typename OPERATION::second_argument_type(a) 
               //       \--- convert via constructor ---/
           );
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# The goal statement (revisited)

## The end result

After the inlining, and optimizations performed by the compiler, the
statement

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
for_each( the_list.begin(), the_list.end(),
	  bind2nd( ptr_fun( custom_fun ), false )
        );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

ends up looking more or less like this:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
for ( list<int>::iterator it = the_list.begin(),
                          it != the_list.end(), ++it ) {
    custom_fun( *it , false );
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
