#include <functional>
#include <numeric>
#include <vector>
#include <iostream>

using namespace std;
using namespace std::placeholders;

int main() {
    vector<int> v = {1, 2, 3, 4, 5};

    int mystery = 
        accumulate( v.begin(), v.end(), 0,
            bind( []( int c, int b, int a ){ return a + b*b + c*c*c; },
                0, _2, _1
            )
        );

    cout << "Mystery value: " << mystery << ".\n";
    
    return 0;
}

/**
    OUTPUT:

        Mystery value: 55.

**/
