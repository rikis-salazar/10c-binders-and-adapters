# What else?

## Missing

 *  Transfer advanced examples from old repository.
 *  Transform the .cpp handout binders_and_adapters.cpp into MarkDown + pdf.
 *  ~~Add slides from the binders_and_adpaters handout~~

## Non-essential

 *  Produce _fancy_ slides using reveal.js and/or similar tools.
 *  Fix typos [if any].
 *  Have a list of the included files at the top of the readme file.
